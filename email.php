<?php

include_once('PHPMailer-master/src/PHPMailer.php');
include_once('PHPMailer-master/src/SMTP.php');

if (isset($_POST["name"]) && isset($_POST["phonenumber"])) {

    // Формируем массив для JSON ответа
    $result = array(
        'name' => $_POST["name"],
        'phonenumber' => $_POST["phonenumber"]
    );

    // Переводим массив в JSON
    $arr = json_encode($result);
}

$name = $result['name'];
$phone = $result['phonenumber'];

$msj = $name . " is waiting your call. Please call him. " . $name . "'s phone is " . $phone;
$mail = new PHPMailer\PHPMailer\PHPMailer();
$mail->IsSMTP();
$mail->SMTPDebug = 1;
//authentication SMTP enabled
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'ssl';
$mail->Host = "smtp.gmail.com";
//Gmail 465 or 587
$mail->Port = 465;
$mail->Username = "ruslan.ismailov.u@gmail.com";
$mail->Password = "***********";
$mail->SetFrom("ruslan.ismailov.u@gmail.com", "Landing BOT");
$mail->Subject = "Email from " . $name;
$mail->MsgHTML($msj);
$mail->AddAddress("chifek@gmail.com");

if ($mail->Send()) {
    print json_encode("SUCCESS");
} else {
    echo "Mailer Error: " . $mail->ErrorInfo;
}